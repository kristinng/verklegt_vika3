#ifndef ADDPCONNECTIONDIALOG_H
#define ADDPCONNECTIONDIALOG_H

#include <QDialog>
#include "person.h"
#include "personservice.h"
#include "computer.h"
#include "computerservice.h"
#include "linkerservice.h"


namespace Ui {
class addpconnectiondialog;
}

class addpconnectiondialog : public QDialog
{
    Q_OBJECT

public:
    explicit addpconnectiondialog(QWidget *parent = 0);
    ~addpconnectiondialog();
    string currentname;
    string currentgender;
    int currentbirthyear;
    int currentdeathyear;
    personservice Personservice;
    computerservice Computerservice;
    linkerservice Linkerservice;
    list <computer> currentcomputers;

    void addpersonconnection(person p);
private slots:
    void on_table_computer_connect_doubleClicked(const QModelIndex &index);

private:
    Ui::addpconnectiondialog *ui;
};

#endif // ADDPCONNECTIONDIALOG_H
