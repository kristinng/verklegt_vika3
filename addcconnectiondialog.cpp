#include "addcconnectiondialog.h"
#include "ui_addcconnectiondialog.h"

addCconnectiondialog::addCconnectiondialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addCconnectiondialog)
{
    ui->setupUi(this);
}

addCconnectiondialog::~addCconnectiondialog()
{
    delete ui;
}
/* creates the table that shows the user all possible options
 * to connect to his computer
 */
void addCconnectiondialog::addcomputerconnection(computer c){
    currentCname=c.name;
    currenttype=c.type;
    currentbuilt=c.built;
    currentbuiltyear=c.builtyear;

    int i=0;
    int currentsortby = 1;
    string namefilter= "";
    string genderfilter= "";
    int birthyearfilter = 0;
    int deathyearfilter = 0;

    currentpersons = Personservice.searchpersonbyfilter(currentsortby, namefilter, genderfilter, birthyearfilter,deathyearfilter );
    ui->table_personconnect->clearContents();
    ui->table_personconnect->setRowCount(currentpersons.size());
    ui->table_personconnect->setColumnWidth(0,200);
    for(list<person>::iterator iter = currentpersons.begin(); iter != currentpersons.end(); iter++){
        ui->table_personconnect->setItem(i,0,new QTableWidgetItem(QString::fromStdString(iter->name)));
        ui->table_personconnect->setItem(i,1,new QTableWidgetItem(QString::fromStdString(iter->gender)));
        ui->table_personconnect->setItem(i,2,new QTableWidgetItem(QString::number(iter->birthyear)));
        ui->table_personconnect->setItem(i,3,new QTableWidgetItem(QString::number(iter->deathyear)));

        i++;
    }
}
/* this lets the user double click the person on the list
 * then that person gets connected to the computer he had already picked
 */
void addCconnectiondialog::on_table_personconnect_doubleClicked(const QModelIndex &index)
{
    computer c = computer();
    c.name = currentCname;
    c.type = currenttype;
    c.built = currentbuilt;
    c.builtyear = currentbuiltyear;

    int i=0;
    person p= person();
    for(list<person>::iterator iter = currentpersons.begin(); iter != currentpersons.end(); iter++){
        if(i==index.row()){
            p.name=iter->name;
            p.gender=iter->gender;
            p.birthyear=iter->birthyear;
            p.deathyear=iter->deathyear;
        }
        i++;
    }
    Linkerservice.connectCandP(p,c);
    this->close();
}
