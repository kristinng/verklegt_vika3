#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //this->setStyleSheet("background-color:white;");
    ui->setupUi(this);
    QPixmap bkgnd("C:/Users/Helena/Documents/Vika31/verklegt_vika3/bkp");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
   // Personservice = personservice();
   // Computerservice = computerservice();
   // Linkerservice = linkerservice();
   // Utilities = utilities();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_showsearchButton_clicked()
{
    showsearchDialog showsearchdialog;
    showsearchdialog.exec();

}

void MainWindow::on_Addbutton_clicked()
{
    ADD_Dialog add_dialog;
    add_dialog.exec();
}

void MainWindow::on_pushButton_clicked()
{
    removeDialog REMOVE;
    REMOVE.exec();
}
