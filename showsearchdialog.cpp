#include "showsearchdialog.h"
#include "ui_showsearchdialog.h"
#include "display_person_dialog.h"
#include "display_computer_dialog.h"
#include <QDebug>
#include <QMenu>

/* most of these functions are triggers for
 * the filtering of the list
 */
showsearchDialog::showsearchDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::showsearchDialog)
{
    ui->setupUi(this);
    sortcomboboxsetup();
    computersortcomboboxsetup();
    ui->computerframe->hide();
    //ui->personframe->hide();
    ui->showpersons_radiobutton->setChecked(true);
    //ui->built_checkBox->setChecked(true);


}

showsearchDialog::~showsearchDialog()
{
    delete ui;
}
/* setting up a table of all the persons in our database
 * we always call the filter function wich sends us a filtered list
 * if there are no filters then it automatically allows everything
 */
void showsearchDialog::getallpersons(){
    int i=0;
    int currentsortby = ui->sortby_combobox->itemData(ui->sortby_combobox->currentIndex()).toInt();
    string namefilter= ui->Namefilter->text().toStdString();
    string genderfilter= ui->Genderfilter->text().toStdString();
    int birthyearfilter = ui->Birthyearfilter->text().toInt();
    int deathyearfilter = ui->Deathyearfilter->text().toInt();
    //qDebug() << currentsortby;
    currentpersons.clear();
    currentpersons = Personservice.searchpersonbyfilter(currentsortby, namefilter, genderfilter, birthyearfilter,deathyearfilter );
    ui->table_person->clearContents();
    ui->table_person->setRowCount(currentpersons.size());
    ui->table_person->setColumnWidth(0,200);
    for(list<person>::iterator iter = currentpersons.begin(); iter != currentpersons.end(); iter++){
        ui->table_person->setItem(i,0,new QTableWidgetItem(QString::fromStdString(iter->name)));
        ui->table_person->setItem(i,1,new QTableWidgetItem(QString::fromStdString(iter->gender)));
        ui->table_person->setItem(i,2,new QTableWidgetItem(QString::number(iter->birthyear)));
        ui->table_person->setItem(i,3,new QTableWidgetItem(QString::number(iter->deathyear)));

        i++;
    }
    ui->table_person->setRowCount(currentpersons.size());
}
/* same as above
 *
 */
void showsearchDialog::getallcomputers(){
    int i=0,builtfilter;
    int currentsortby = ui->Csortby_combobox->itemData(ui->Csortby_combobox->currentIndex()).toInt();
    string namefilter= ui->Cnamefilter->text().toStdString();
    string typefilter= ui->typefilter->text().toStdString();
    if((ui->built_checkBox->isChecked()==true) &(ui->Notbuilt_checkBox->isChecked()==true)){
        //builtfilter=1;
    }
    else if(ui->built_checkBox->isChecked()==true){
        builtfilter=1;
    }
    else if(ui->Notbuilt_checkBox->isChecked()==true){
        builtfilter=0;
    }
    else if((ui->built_checkBox->isChecked()==false) &(ui->Notbuilt_checkBox->isChecked()==false)){
        builtfilter=-1;
    }
    int builtyearfilter = ui->Builtyearfilter->text().toInt();
    //qDebug() << currentsortby;
    currentcomputers.clear();
    currentcomputers = Computerservice.searchcomputerbyfilter(currentsortby, namefilter, typefilter, builtfilter,builtyearfilter );
    ui->table_computer->clearContents();
    ui->table_computer->setRowCount(currentcomputers.size());
    ui->table_computer->setColumnWidth(0,200);
    for(list<computer>::iterator iter = currentcomputers.begin(); iter != currentcomputers.end(); iter++){
        ui->table_computer->setItem(i,0,new QTableWidgetItem(QString::fromStdString(iter->name)));
        ui->table_computer->setItem(i,1,new QTableWidgetItem(QString::fromStdString(iter->type)));
        ui->table_computer->setItem(i,2,new QTableWidgetItem(QString::number(iter->built)));
        ui->table_computer->setItem(i,3,new QTableWidgetItem(QString::number(iter->builtyear)));

        i++;
    }
    ui->table_computer->setRowCount(currentcomputers.size());
}

void showsearchDialog::sortcomboboxsetup(){

    ui->sortby_combobox->addItem("Name",QVariant(1));
    ui->sortby_combobox->addItem("Gender",QVariant(2));
    ui->sortby_combobox->addItem("Birthyear",QVariant(3));
    ui->sortby_combobox->addItem("Deathyear",QVariant(4));
    ui->sortby_combobox->addItem("Name reversed",QVariant(5));
    ui->sortby_combobox->addItem("Gender reversed",QVariant(6));
    ui->sortby_combobox->addItem("Birthyear reversed",QVariant(7));
    ui->sortby_combobox->addItem("Deathyear reversed",QVariant(8));
}
void showsearchDialog::computersortcomboboxsetup(){

    ui->Csortby_combobox->addItem("Name",QVariant(1));
    ui->Csortby_combobox->addItem("Type",QVariant(2));
    ui->Csortby_combobox->addItem("Built",QVariant(3));
    ui->Csortby_combobox->addItem("Builtyear",QVariant(4));
    ui->Csortby_combobox->addItem("Name DESC",QVariant(5));
    ui->Csortby_combobox->addItem("Type DESC",QVariant(6));
    ui->Csortby_combobox->addItem("Built DESC",QVariant(7));
    ui->Csortby_combobox->addItem("Builtyear DESC",QVariant(8));
}
void showsearchDialog::on_showpersons_radiobutton_toggled()
{
    getallpersons();
}

void showsearchDialog::on_Namefilter_textChanged()
{
    getallpersons();
}

void showsearchDialog::on_Genderfilter_textEdited()
{
    getallpersons();
}

void showsearchDialog::on_Birthyearfilter_textEdited()
{
    getallpersons();
}

void showsearchDialog::on_Deathyearfilter_textEdited()
{
    getallpersons();
}

void showsearchDialog::on_sortby_combobox_currentIndexChanged()
{
    getallpersons();
}

void showsearchDialog::on_showcomputer_radiobutton_toggled()
{

}
/* these functions switch depending
 * on what the user has clicked on in the ui
 */
void showsearchDialog::on_showpersons_radiobutton_clicked()
{
    ui->computerframe->hide();
    ui->personframe->show();
    getallcomputers();


}

void showsearchDialog::on_showcomputer_radiobutton_clicked()
{
    ui->computerframe->show();
    ui->personframe->hide();
    getallpersons();

}

void showsearchDialog::on_built_checkBox_toggled()
{
    getallcomputers();
}

void showsearchDialog::on_Notbuilt_checkBox_toggled()
{
    getallcomputers();
}

void showsearchDialog::on_Builtyearfilter_textEdited()
{
    getallcomputers();
}

void showsearchDialog::on_typefilter_textEdited()
{
    getallcomputers();
}

void showsearchDialog::on_Cnamefilter_textEdited()
{
    getallcomputers();
}

void showsearchDialog::on_Csortby_combobox_currentIndexChanged()
{
    getallcomputers();
}
void showsearchDialog::on_Person_filter_pushButton_clicked(){

}
/* if a person or computer on the list
 * is double clicked on it will open a profile window of sorts
 */
void showsearchDialog::on_table_computer_doubleClicked(const QModelIndex &index)
{
    int i=0;
    computer c= computer();
    display_computer_dialog displaycomputerdialog;
    for(list<computer>::iterator iter = currentcomputers.begin(); iter != currentcomputers.end(); iter++){
        if(i==index.row()){
            c.name=iter->name;
            c.type=iter->type;
            c.built=iter->built;
            c.builtyear=iter->builtyear;
        }
        i++;
    }
    displaycomputerdialog.setcomputer(c);
    displaycomputerdialog.exec();

}

void showsearchDialog::on_table_person_doubleClicked(const QModelIndex &index)
{
    int i=0;
    person p= person();
    display_person_Dialog displaypersondialog;
    for(list<person>::iterator iter = currentpersons.begin(); iter != currentpersons.end(); iter++){
        if(i==index.row()){
            p.name=iter->name;
            p.gender=iter->gender;
            p.birthyear=iter->birthyear;
            p.deathyear=iter->deathyear;
        }
        i++;
    }
    displaypersondialog.setperson(p);
    displaypersondialog.exec();


}

void showsearchDialog::on_table_person_customContextMenuRequested(const QPoint &pos)
{
    QMenu menu;
    menu.addAction(ui->action_Remove_from_list);

    menu.exec(ui->table_person->viewport()->mapToGlobal(pos));
}

