#ifndef LINKERSERVICE_H
#define LINKERSERVICE_H
#include "linkerrepository.h"

class linkerservice
{
public:
    linkerservice();
    void connectCandP(person p, computer c);
    linkerrepository Linkerrepository;
    list<computer> getconnectedcomputers(person p);
    list<person> getconnectedpersons(computer c);
};

#endif // LINKERSERVICE_H
