#include "addpconnectiondialog.h"
#include "ui_addpconnectiondialog.h"

addpconnectiondialog::addpconnectiondialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addpconnectiondialog)
{
    ui->setupUi(this);
}

addpconnectiondialog::~addpconnectiondialog()
{
    delete ui;
}
/* see addcconnectiondialog for explanation
 * of all functions
 * only difference is computer instead of person
 * all functionality is basically the same
 */
void addpconnectiondialog::addpersonconnection(person p){


    currentname = p.name;
    currentgender = p.gender;
    currentbirthyear = p.birthyear;
    currentdeathyear = p.deathyear;

    int i=0;
    int builtfilter=-1;
    int currentsortby =1;
    string namefilter= "";
    string typefilter= "";
    int builtyearfilter = 0;
    currentcomputers = Computerservice.searchcomputerbyfilter(currentsortby, namefilter, typefilter, builtfilter,builtyearfilter );
    ui->table_computer_connect->clearContents();
    ui->table_computer_connect->setRowCount(currentcomputers.size());
    ui->table_computer_connect->setColumnWidth(0,200);
    for(list<computer>::iterator iter = currentcomputers.begin(); iter != currentcomputers.end(); iter++){
        ui->table_computer_connect->setItem(i,0,new QTableWidgetItem(QString::fromStdString(iter->name)));
        ui->table_computer_connect->setItem(i,1,new QTableWidgetItem(QString::fromStdString(iter->type)));
        ui->table_computer_connect->setItem(i,2,new QTableWidgetItem(QString::number(iter->built)));
        ui->table_computer_connect->setItem(i,3,new QTableWidgetItem(QString::number(iter->builtyear)));

        i++;
    }
}

void addpconnectiondialog::on_table_computer_connect_doubleClicked(const QModelIndex &index)
{
    person p = person();
    p.name = currentname;
    p.gender = currentgender;
    p.birthyear = currentbirthyear;
    p.deathyear = currentdeathyear;

    int i=0;
    computer c= computer();
    for(list<computer>::iterator iter = currentcomputers.begin(); iter != currentcomputers.end(); iter++){
        if(i==index.row()){
            c.name=iter->name;
            c.type=iter->type;
            c.built=iter->built;
            c.builtyear=iter->builtyear;
        }
        i++;
    }
    Linkerservice.connectCandP(p,c);
    this->close();

}
