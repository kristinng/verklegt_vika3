#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <string>
#include <stdio.h>
#include <cctype>
#include <stdlib.h>
#include <iomanip>
#include "personservice.h"
#include "add_dialog.h"
#include "showsearchdialog.h"
#include "removedialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_showsearchButton_clicked();
    void on_Addbutton_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    personservice Personservice;
    //computerservice Computerservice;
    //linkerservice Linkerservice;
    //utilities Utilities;
};

#endif // MAINWINDOW_H

/*
#ifndef CONSOLEUI_H
#define CONSOLEUI_H


class ConsoleUI
{
public:
    ConsoleUI();
    void start();
    void showlist(list<person>plist);
    void searchlist();
    void personsortoptions();
    void add();
    void computersortoptions();
    void showlistcomputer(list<computer> clist);
    void connectionoptions();
    void clear();
private:
    personservice Personservice;
    computerservice Computerservice;
    linkerservice Linkerservice;
    utilities Utilities;
};

#endif // CONSOLEUI_H
*/
