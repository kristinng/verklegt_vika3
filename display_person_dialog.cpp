#include "display_person_dialog.h"
#include "ui_display_person_dialog.h"

display_person_Dialog::display_person_Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::display_person_Dialog)
{
    ui->setupUi(this);
}

display_person_Dialog::~display_person_Dialog()
{
    delete ui;
}
/* See the computer version of this code for comments
 * like always they are very much the same
 */
void display_person_Dialog::setperson(person Person){

    currentname=Person.name;
    currentgender=Person.gender;
    currentbirthyear=Person.birthyear;
    currentdeathyear=Person.deathyear;

    ui->label_display_pname_here->setText(QString::fromStdString(Person.name));
    ui->label_dispaly_gender_here->setText(QString::fromStdString(Person.gender));
    ui->label_display_birthyear_here->setText(QString::number(Person.birthyear));
    ui->label_display_deathyear_here->setText(QString::number(Person.deathyear));

    QString a = QString::number(Person.deathyear);
    qDebug() << a;
    QByteArray array=Personservice.getimage(Person);
    QPixmap pixmap = QPixmap();
    pixmap.loadFromData(array);
    ui->label_Pimage->setScaledContents(true);
    ui->label_Pimage->setPixmap(pixmap);
    showconnected_computers();
}

void display_person_Dialog::on_pushButton_addperson_connection_clicked()
{
    person p = person();
    p.name = currentname;
    p.gender = currentgender;
    p.birthyear = currentbirthyear;
    p.deathyear = currentdeathyear;
    addpconnectiondialog AddPconnectiondialog;
    AddPconnectiondialog.addpersonconnection(p);
    AddPconnectiondialog.exec();
    showconnected_computers();
}
void display_person_Dialog::showconnected_computers(){

    int i=0;
    person p = person();
    p.name = currentname;
    p.gender = currentgender;
    p.birthyear = currentbirthyear;
    p.deathyear = currentdeathyear;
    list <computer> connectedcomputers;
    connectedcomputers = Linkerservice.getconnectedcomputers(p);
    ui->table_inventions_worked_on->clearContents();
    ui->table_inventions_worked_on->setRowCount(connectedcomputers.size());
    ui->table_inventions_worked_on->setColumnWidth(0,200);
    for(list<computer>::iterator iter = connectedcomputers.begin(); iter != connectedcomputers.end(); iter++){
        ui->table_inventions_worked_on->setItem(i,0,new QTableWidgetItem(QString::fromStdString(iter->name)));
        ui->table_inventions_worked_on->setItem(i,1,new QTableWidgetItem(QString::fromStdString(iter->type)));
        ui->table_inventions_worked_on->setItem(i,2,new QTableWidgetItem(QString::number(iter->built)));
        ui->table_inventions_worked_on->setItem(i,3,new QTableWidgetItem(QString::number(iter->builtyear)));

        i++;
    }
}
