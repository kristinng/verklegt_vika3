#include "display_computer_dialog.h"
#include "ui_display_computer_dialog.h"

display_computer_dialog::display_computer_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::display_computer_dialog)
{
    ui->setupUi(this);
}

display_computer_dialog::~display_computer_dialog()
{
    delete ui;
}
/* a setup function. we set variables for use in all functions
 * we set labels with info about the computer
 * then we get the blob through getimage function
 * we then use pixmap to display it through a label
 */
void display_computer_dialog::setcomputer(computer Computer){

    currentCname=Computer.name;
    currenttype=Computer.type;
    currentbuilt=Computer.built;
    currentbuiltyear=Computer.builtyear;

    ui->label_display_cname_here->setText(QString::fromStdString(Computer.name));
    ui->label_display_type_here->setText(QString::fromStdString(Computer.type));
    if(Computer.built==1){
        QString yes="Yes";
        ui->label_display_built_here->setText(yes);
    }
    else if(Computer.built==0){
        QString No="No";
        ui->label_display_built_here->setText(No);
    }
    ui->label_display_builtyear_here->setText(QString::number(Computer.builtyear));

    QByteArray array=Computerservice.getimage(Computer);

    QPixmap pixmap = QPixmap();
    pixmap.loadFromData(array);
    ui->label_Cimage->setScaledContents(true);
    ui->label_Cimage->setPixmap(pixmap);
    showconnected_persons();
}
/* simply inserts all people who have are connected
 * to this computer into a table
 */
void display_computer_dialog::showconnected_persons(){

    int i=0;
    computer c = computer();
    c.name = currentCname;
    c.type = currenttype;
    c.built = currentbuilt;
    c.builtyear = currentbuiltyear;
    list <person> connectedpersons;
    connectedpersons = Linkerservice.getconnectedpersons(c);
    ui->table_inventors_worked_on->clearContents();
    ui->table_inventors_worked_on->setRowCount(connectedpersons.size());
    ui->table_inventors_worked_on->setColumnWidth(0,200);
    for(list<person>::iterator iter = connectedpersons.begin(); iter != connectedpersons.end(); iter++){
        ui->table_inventors_worked_on->setItem(i,0,new QTableWidgetItem(QString::fromStdString(iter->name)));
        ui->table_inventors_worked_on->setItem(i,1,new QTableWidgetItem(QString::fromStdString(iter->gender)));
        ui->table_inventors_worked_on->setItem(i,2,new QTableWidgetItem(QString::number(iter->birthyear)));
        ui->table_inventors_worked_on->setItem(i,3,new QTableWidgetItem(QString::number(iter->deathyear)));
        i++;
    }
}
/* offer the user an option to add connections
 * we pass the computer to the next function
 * to know wich computer we are adding connection to
 */
void display_computer_dialog::on_pushButton_addcomputer_connection_clicked()
{
    computer c = computer();
    c.name = currentCname;
    c.type = currenttype;
    c.built = currentbuilt;
    c.builtyear = currentbuiltyear;
    addCconnectiondialog Addcconnectiondialog;
    Addcconnectiondialog.addcomputerconnection(c);
    Addcconnectiondialog.exec();
    showconnected_persons();
}
