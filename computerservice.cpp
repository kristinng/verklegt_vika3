#include "computerservice.h"

computerservice::computerservice()
{
    computerrep=computerrepos();
}
void computerservice::addcomputer(computer c)
{
    computerrep.addcomputer(c);
}
list<computer>computerservice::searchcomputerbyfilter(int currentsortby, string namefilter, string typefilter, int builtfilter, int builtyearfilter ){
    return computerrep.searchcomputerbyfilter(currentsortby, namefilter, typefilter, builtfilter,builtyearfilter );
}
QByteArray computerservice::getimage(computer c){
    return computerrep.getimage(c);
}
void computerservice::removefromdb(string removecomputername){
    computerrep.removefromdb(removecomputername);
}
