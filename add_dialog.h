#ifndef ADD_DIALOG_H
#define ADD_DIALOG_H

#include <QDialog>
#include <personservice.h>
#include <QFileDialog>
#include <computerservice.h>
#include "utilities.h"
#include <QString>

namespace Ui {
class ADD_Dialog;
}

class ADD_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit ADD_Dialog(QWidget *parent = 0);
    ~ADD_Dialog();
private slots:
    void on_Cancel_Button_clicked();
    void on_Addp_button_clicked();
    void on_imagepath_button_clicked();
    void on_c_imagepath_button_clicked();
    void on_C_Cancel_Button_clicked();
    void on_Addc_button__clicked();

private:
    computerservice Computerservice;
    personservice Personservice;
    utilities  Utilities;
    Ui::ADD_Dialog *ui;
};

#endif // ADD_DIALOG_H
