#ifndef LINKERREPOSITORY_H
#define LINKERREPOSITORY_H
#include "person.h"
#include "computer.h"
#include <QTsql>
#include <QString>
class linkerrepository
{
public:
    linkerrepository();
    void connectCandP(person p, computer c);
    QSqlDatabase db;
    QString getLastExecutedQuery(const QSqlQuery &query);
    list<computer> getconnectedcomputers(person p);
    list<person> getconnectedpersons(computer c);
};

#endif // LINKERREPOSITORY_H
