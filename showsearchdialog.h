#ifndef SHOWSEARCHDIALOG_H
#define SHOWSEARCHDIALOG_H

#include <QDialog>
#include <personservice.h>
#include <computerservice.h>
#include <QItemSelectionModel>

namespace Ui {
class showsearchDialog;
}

class showsearchDialog : public QDialog
{
    Q_OBJECT

public:
    explicit showsearchDialog(QWidget *parent = 0);
    ~showsearchDialog();

    void getallpersons();
    void sortcomboboxsetup();
    void getallcomputers();
    void computersortcomboboxsetup();
private slots:
    void on_Person_filter_pushButton_clicked();
    void on_showpersons_radiobutton_toggled();
    void on_Namefilter_textChanged();
    void on_Genderfilter_textEdited();
    void on_Birthyearfilter_textEdited();
    void on_Deathyearfilter_textEdited();
    void on_sortby_combobox_currentIndexChanged();
    void on_showcomputer_radiobutton_toggled();
    void on_showpersons_radiobutton_clicked();
    void on_showcomputer_radiobutton_clicked();


    void on_built_checkBox_toggled();

    void on_Notbuilt_checkBox_toggled();

    void on_Builtyearfilter_textEdited();

    void on_typefilter_textEdited();

    void on_Cnamefilter_textEdited();

    void on_Csortby_combobox_currentIndexChanged();

    void on_table_computer_doubleClicked(const QModelIndex &index);

    void on_table_person_doubleClicked(const QModelIndex &index);

    void on_table_person_customContextMenuRequested(const QPoint &pos);



private:
    Ui::showsearchDialog *ui;
    personservice Personservice;
    computerservice Computerservice;
    list<person>currentpersons;
    list<computer>currentcomputers;

};

#endif // SHOWSEARCHDIALOG_H
