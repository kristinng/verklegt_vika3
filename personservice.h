#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H
#include "person.h"
#include "personrepos.h"
#include <list>
#include <algorithm>

class personservice
{
public:
    personservice();  
    void addperson(person p);
    list<person> searchpersonbyfilter(int currentsortby, string namefilter, string genderfilter, int birthyearfilter, int deathyearfilter);
    QByteArray getimage(person p);
    void removefromdb(string removepersonname);
private:
    personrepos personrep;
};

#endif // PERSONSERVICE_H
