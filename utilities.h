#ifndef UTILITIES_H
#define UTILITIES_H

#include <iostream>
#include <string>
#include <QMessageBox>


class utilities
{
public:
    utilities();
    bool is_number(const std::string &s);
    bool contains_number(const std::string &s);
    bool is_empty(const std::string &s);
};

#endif // UTILITIES_H
