#include "computerrepos.h"
#include <QDebug>
/* opening the data connection
 * and making sure there are no duplicates
 */
computerrepos::computerrepos()
{
    computerlist = std::list<computer>();

    QString connectionName = "dataConnection";
    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("database.sqlite");
    }
}
/* pretty basic sql command to insert into the db
 * with exception of image, wich we input as a blob
 * bytearray is created and file is read as such
 */
void computerrepos::addcomputer(computer c){//this function simply takes in a computer and pushes it into the list and then

    db.open();
    QByteArray byteArray;
    if(c.imagepath==""){

    }
    else{
        QString imagefile= QString::fromStdString(c.imagepath);
        QFile file(imagefile);
        if (!file.open(QIODevice::ReadOnly)) return;
        byteArray = file.readAll();
    }
    QSqlQuery query(db);
    query.prepare("INSERT INTO computer VALUES (NULL, :name, :type, :built, :builtyear, :imagepath)");
    query.bindValue(":name", QString::fromStdString(c.name));
    query.bindValue(":type", QString::fromStdString(c.type));
    query.bindValue(":built",c.built);
    if(c.builtyear==0){
        query.bindValue(":builtyear", "NULL");
    }
    else{
        query.bindValue(":builtyear", c.builtyear);
    }
    if(byteArray.isEmpty()){
        QString b= "null";
        query.bindValue(":imagepath", b);
    }
    else if(!byteArray.isEmpty()){
        query.bindValue(":imagepath", byteArray);
    }
    query.exec();
    db.close();

}
/* probably the most fun feature to see work
 * It filters by all of the input filters and sorts at real time
 * it is accomplished by the use of wildcards and updating the list
 * everytime lineedit changes or for example the sort combox
 */
list <computer> computerrepos::searchcomputerbyfilter( int currentsortby, string namefilter, string typefilter, int builtfilter, int builtyearfilter ){
    string sortby;
    list<computer>templist;
    db.open();
    QSqlQuery query(db);
    switch(currentsortby){
    case 1:{
        query.prepare("SELECT Name, Type, Built, Builtyear "
                      "FROM computer WHERE Name LIKE (:name) AND Type LIKE (:type) "
                      "AND Built LIKE (:built) AND Builtyear LIKE (:builtyear) "
                      "ORDER BY Name");
        break;
        }
    case 2:{
        query.prepare("SELECT Name, Type, Built, Builtyear "
                      "FROM computer WHERE Name LIKE (:name) AND Type LIKE (:type) "
                      "AND Built LIKE (:built) AND Builtyear LIKE (:builtyear) "
                      "ORDER BY Type");
        break;
        }
    case 3:{
        query.prepare("SELECT Name, Type, Built, Builtyear "
                      "FROM computer WHERE Name LIKE (:name) AND Type LIKE (:type) "
                      "AND Built LIKE (:built) AND Builtyear LIKE (:builtyear) "
                      "ORDER BY Built");
        break;
        }
    case 4:{
        query.prepare("SELECT Name, Type, Built, Builtyear "
                      "FROM computer WHERE Name LIKE (:name) AND Type LIKE (:type) "
                      "AND Built LIKE (:built) AND Builtyear LIKE (:builtyear) "
                      "ORDER BY Builtyear");
        break;
        }
    case 5:{
        query.prepare("SELECT Name, Type, Built, Builtyear "
                      "FROM computer WHERE Name LIKE (:name) AND Type LIKE (:type) "
                      "AND Built LIKE (:built) AND Builtyear LIKE (:builtyear) "
                      "ORDER BY Name DESC");
        break;
        }
    case 6:{
        query.prepare("SELECT Name, Type, Built, Builtyear "
                      "FROM computer WHERE Name LIKE (:name) AND Type LIKE (:type) "
                      "AND Built LIKE (:built) AND Builtyear LIKE (:builtyear) "
                      "ORDER BY Type DESC");
        break;
        }
    case 7:{
        query.prepare("SELECT Name, Type, Built, Builtyear "
                      "FROM computer WHERE Name LIKE (:name) AND Type LIKE (:type) "
                      "AND Built LIKE (:built) AND Builtyear LIKE (:builtyear) "
                      "ORDER BY Built DESC");
        break;
        }
    case 8:{
        query.prepare("SELECT Name, Type, Built, Builtyear "
                      "FROM computer WHERE Name LIKE (:name) AND Type LIKE (:type) "
                      "AND Built LIKE (:built) AND Builtyear LIKE (:builtyear) "
                      "ORDER BY Builtyear DESC");
        break;
        }
    }
    query.bindValue(":name", QString::fromStdString("%"+namefilter+"%"));
    query.bindValue(":type", QString::fromStdString("%"+typefilter+"%"));
    if(builtfilter==-1){
        string builtandnot="%";
        query.bindValue(":built", QString::fromStdString(builtandnot));
    }
    else{
        query.bindValue(":built", (QString::number(builtfilter)));
    }
    QString stringbuiltyearfilter=QString::number(builtyearfilter);
    if(stringbuiltyearfilter=="0"){
        stringbuiltyearfilter="";
        query.bindValue(":builtyear", (stringbuiltyearfilter+"%"));
    }
    else{
        query.bindValue(":builtyear", (stringbuiltyearfilter+"%"));
    }
    query.exec();
    while(query.next()){
        computer c = computer();
        c.name = query.value("Name").toString().toStdString();
        c.type = query.value("Type").toString().toStdString();
        c.built = query.value("Built").toInt();
        c.builtyear = query.value("Builtyear").toInt();
        templist.push_back(c);
    }
    db.close();
    return templist;
}
/* a debugging function
 * it prints out the sql command in Qstring
 * this was found on the internet to help debug
 */
QString computerrepos::getLastExecutedQuery(const QSqlQuery& query)
{
 QString str = query.lastQuery();
 QMapIterator<QString, QVariant> it(query.boundValues());
 while (it.hasNext())
 {
  it.next();
  str.replace(it.key(),it.value().toString());
 }
 return str;
}
/* this function is called when we want to display an image
 * we get the blob from the database
 * and pass it with a function of type Qbytearray
 */
QByteArray computerrepos::getimage(computer c){

    db.open();

    QSqlQuery query(db);
    QByteArray array;
    query.prepare("SELECT Image "
               "FROM computer WHERE Name = (:name) AND Type = (:type) "
               "AND Built = (:built) AND Builtyear = (:builtyear)");
    query.bindValue(":name", QString::fromStdString(c.name));
    query.bindValue(":type", QString::fromStdString(c.type));
    query.bindValue(":built", QString::number(c.built));
    query.bindValue(":builtyear", QString::number(c.builtyear));
    query.exec();
    while(query.next()){
        array = query.value("Image").toByteArray();
    };

    db.close();
    return array;
}
/* VERY simple remove command
 * takes a input string and if it matches it exactly
 * then it will delete that row from the database
 * user is warned about consequences and about only matching EXACT name
 */
void computerrepos::removefromdb(string removecomputername){

    db.open();
    QSqlQuery query(db);
    query.prepare("DELETE FROM computer WHERE Name = (:name)");
    query.bindValue(":name", QString::fromStdString(removecomputername));
    query.exec();
    db.close();

}
