#ifndef COMPUTERREPOS_H
#define COMPUTERREPOS_H
#include <list>
#include "computer.h"
#include <fstream>
#include <iostream>
#include <QTsql>
#include <QString>



class computerrepos
{
public:
    computerrepos();
    QSqlDatabase db;
    std::list<computer> computerlist;
    void addcomputer(computer p);
    list<computer> searchcomputerbyfilter(int currentsortby, string namefilter, string typefilter, int builtfilter, int builtyearfilter);
    QByteArray getimage(computer c);
    QString getLastExecutedQuery(const QSqlQuery &query);
    void removefromdb(string removecomputername);
};

#endif // LINKERREPOS_H
