#include "personservice.h"

personservice::personservice()
{
    personrep = personrepos();
}
void personservice::addperson(person p)
{
    personrep.addperson(p);
}
list<person>personservice::searchpersonbyfilter(int currentsortby, string namefilter, string genderfilter, int birthyearfilter, int deathyearfilter ){
    return personrep.searchpersonbyfilter(currentsortby, namefilter, genderfilter, birthyearfilter,deathyearfilter );
}
QByteArray personservice::getimage(person p){
    return personrep.getimage(p);
}
void personservice::removefromdb(string removepersonname){
    personrep.removefromdb(removepersonname);
}
