#ifndef COMPUTERSERVICE_H
#define COMPUTERSERVICE_H

#include "computer.h"
#include "computerrepos.h"
#include <list>
#include <algorithm>

class computerservice
{
public:
    computerservice();
    void addcomputer(computer c);
    list<computer> searchcomputerbyfilter(int currentsortby, string namefilter, string typefilter, int builtfilter, int builtyearfilter);

    QByteArray getimage(computer c);
    void removefromdb(string removecomputername);
private:
    computerrepos computerrep;
};

#endif // COMPUTERSERVICE_H
