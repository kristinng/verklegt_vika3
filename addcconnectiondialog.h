#ifndef ADDCCONNECTIONDIALOG_H
#define ADDCCONNECTIONDIALOG_H

#include <QDialog>
#include "computer.h"
#include "linkerservice.h"
#include "personservice.h"
#include "computerservice.h"

namespace Ui {
class addCconnectiondialog;
}

class addCconnectiondialog : public QDialog
{
    Q_OBJECT

public:
    explicit addCconnectiondialog(QWidget *parent = 0);
    ~addCconnectiondialog();
    string currentCname;
    string currenttype;
    int currentbuilt;
    int currentbuiltyear;
    list <person> currentpersons;

    void addcomputerconnection(computer c);
private slots:
    void on_table_personconnect_doubleClicked(const QModelIndex &index);

private:
    Ui::addCconnectiondialog *ui;
    personservice Personservice;
    computerservice Computerservice;
    linkerservice Linkerservice;
};

#endif // ADDCCONNECTIONDIALOG_H
