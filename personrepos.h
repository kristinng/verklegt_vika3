#ifndef PERSONREPOS_H
#define PERSONREPOS_H
#include <list>
#include "person.h"
#include <fstream>
#include <iostream>
#include <QTsql>
#include <QString>

class personrepos
{
public:
    personrepos();
    QSqlDatabase db;
    std::list<person> personlist;
    void addperson(person p);
    list<person> searchpersonbyfilter(int currentsortby, string namefilter, string genderfilter, int birthyearfilter, int deathyearfilter);
    QString getLastExecutedQuery(const QSqlQuery &query);
    QByteArray getimage(person p);
    void removefromdb(string removepersonname);
};

#endif // PERSONREPOS_H
