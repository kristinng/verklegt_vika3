#include "personrepos.h"
#include <QDebug>
#include <string>
#include <QFile>
#include <QString>
#include <QFileDialog>

/* see computerrepos for comments
 * like always it is very much the same
 */
personrepos::personrepos()
{
    personlist = std::list<person>();

    QString connectionName = "dataConnection";
    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("database.sqlite");
    }
}

void personrepos::addperson(person p){
    db.open();
    QByteArray byteArray;
    if(p.imagepath==""){

    }
    else{
        QString imagefile= QString::fromStdString(p.imagepath);
        QFile file(imagefile);
        if (!file.open(QIODevice::ReadOnly)) return;
        byteArray = file.readAll();
    }

    QSqlQuery query(db);
    query.prepare("INSERT INTO person VALUES (NULL, :name, :gender, :birthyear, :deathyear, :image)");
    query.bindValue(":name", QString::fromStdString(p.name));
    query.bindValue(":gender", QString::fromStdString(p.gender));
    query.bindValue(":birthyear", p.birthyear);
    if(p.deathyear==0){
        query.bindValue(":deathyear", QString::fromStdString("NULL"));
    }
    else{
        query.bindValue(":deathyear", p.deathyear);
    }
    if(byteArray.isEmpty()){
        QString b= "null";
        query.bindValue(":image", b);
    }
    else if(!byteArray.isEmpty()){
        query.bindValue(":image", byteArray);
    }
    query.exec();
    db.close();
}
list <person> personrepos::searchpersonbyfilter( int currentsortby, string namefilter, string genderfilter, int birthyearfilter, int deathyearfilter ){
    string sortby;
    list<person>templist;
    db.open();
    QSqlQuery query(db);
    switch(currentsortby){
    case 1:{
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear "
                      "FROM person WHERE Name LIKE (:name) AND Gender LIKE (:gender) "
                      "AND Birthyear LIKE (:birthyear) AND Deathyear LIKE (:deathyear) "
                      "ORDER BY Name");
        break;
        }
    case 2:{
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear "
                      "FROM person WHERE Name LIKE (:name) AND Gender LIKE (:gender) "
                      "AND Birthyear LIKE (:birthyear) AND Deathyear LIKE (:deathyear) "
                      "ORDER BY Gender");
        break;
        }
    case 3:{
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear "
                      "FROM person WHERE Name LIKE (:name) AND Gender LIKE (:gender) "
                      "AND Birthyear LIKE (:birthyear) AND Deathyear LIKE (:deathyear) "
                      "ORDER BY Birthyear");
        break;
        }
    case 4:{
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear "
                      "FROM person WHERE Name LIKE (:name) AND Gender LIKE (:gender) "
                      "AND Birthyear LIKE (:birthyear) AND Deathyear LIKE (:deathyear) "
                      "ORDER BY Deathyear");
        break;
        }
    case 5:{
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear "
                      "FROM person WHERE Name LIKE (:name) AND Gender LIKE (:gender) "
                      "AND Birthyear LIKE (:birthyear) AND Deathyear LIKE (:deathyear) "
                      "ORDER BY Name DESC");
        break;
        }
    case 6:{
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear "
                      "FROM person WHERE Name LIKE (:name) AND Gender LIKE (:gender) "
                      "AND Birthyear LIKE (:birthyear) AND Deathyear LIKE (:deathyear) "
                      "ORDER BY Gender DESC");
        break;
        }
    case 7:{
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear "
                      "FROM person WHERE Name LIKE (:name) AND Gender LIKE (:gender) "
                      "AND Birthyear LIKE (:birthyear) AND Deathyear LIKE (:deathyear) "
                      "ORDER BY Birthyear DESC");
        break;
        }
    case 8:{
        query.prepare("SELECT Name, Gender, Birthyear, Deathyear "
                      "FROM person WHERE Name LIKE (:name) AND Gender LIKE (:gender) "
                      "AND Birthyear LIKE (:birthyear) AND Deathyear LIKE (:deathyear) "
                      "ORDER BY Deathyear DESC");
        break;
        }
    }
    query.bindValue(":name", QString::fromStdString("%"+namefilter+"%"));
    query.bindValue(":gender", QString::fromStdString(genderfilter+"%"));
    QString stringbirthyearfilter=QString::number(birthyearfilter);
    if(stringbirthyearfilter=="0"){
        stringbirthyearfilter="";
        query.bindValue(":birthyear", (stringbirthyearfilter+"%"));
    }
    else{
        query.bindValue(":birthyear", (stringbirthyearfilter+"%"));
    }
    QString stringdeathyearfilter=QString::number(deathyearfilter);
    if(stringdeathyearfilter=="0"){
        stringdeathyearfilter="";
        query.bindValue(":deathyear", (stringdeathyearfilter+"%"));
    }
    else{
        query.bindValue(":deathyear", (stringdeathyearfilter+"%"));
    }
    query.exec();
    while(query.next()){
        person p = person();
        p.name = query.value("Name").toString().toStdString();
        p.gender = query.value("Gender").toString().toStdString();
        p.birthyear = query.value("Birthyear").toInt();
        p.deathyear = query.value("Deathyear").toInt();
        templist.push_back(p);
    }
    db.close();
    return templist;
}
QByteArray personrepos::getimage(person p){
    db.open();

    QSqlQuery query(db);
    QByteArray array;
    query.prepare("SELECT Image "
               "FROM person WHERE Name = (:name) AND Gender = (:gender) "
               "AND Birthyear = (:birthyear) AND Deathyear = (:deathyear)");

    query.bindValue(":name", QString::fromStdString(p.name));
    query.bindValue(":gender", QString::fromStdString(p.gender));
    query.bindValue(":birthyear", QString::number(p.birthyear));
    query.bindValue(":deathyear", QString::number(p.deathyear));

    query.exec();

    while(query.next()){
        array = query.value("Image").toByteArray();
    };

    db.close();



    return array;
}
QString personrepos::getLastExecutedQuery(const QSqlQuery& query)
{
 QString str = query.lastQuery();
 QMapIterator<QString, QVariant> it(query.boundValues());
 while (it.hasNext())
 {
  it.next();
  str.replace(it.key(),it.value().toString());
 }
 return str;
}
void personrepos::removefromdb(string removepersonname){

    db.open();
    QSqlQuery query(db);
    query.prepare("DELETE FROM person WHERE Name = (:name)");
    query.bindValue(":name", QString::fromStdString(removepersonname));
    query.exec();
    db.close();
}
