#ifndef DISPLAYPERSONDIALOG_H
#define DISPLAYPERSONDIALOG_H

#include <QDialog>

namespace Ui {
class displaypersonDialog;
}

class displaypersonDialog : public QDialog
{
    Q_OBJECT

public:
    explicit displaypersonDialog(QWidget *parent = 0);
    ~displaypersonDialog();

private:
    Ui::displaypersonDialog *ui;
};

#endif // DISPLAYPERSONDIALOG_H
