#ifndef REMOVEDIALOG_H
#define REMOVEDIALOG_H

#include <QDialog>
#include <string>
#include "computerservice.h"
#include "personservice.h"

namespace Ui {
class removeDialog;
}

class removeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit removeDialog(QWidget *parent = 0);
    ~removeDialog();

    personservice Personservice;
    computerservice Computerservice;

private slots:


    void on_pushButton_remove_person_clicked();

    void on_pushButton_remove_computer_clicked();

private:
    Ui::removeDialog *ui;
};

#endif // REMOVEDIALOG_H
