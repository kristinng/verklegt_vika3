#-------------------------------------------------
#
# Project created by QtCreator 2014-12-12T21:59:33
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = verklegtvika3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    person.cpp \
    personservice.cpp \
    personrepos.cpp \
    computer.cpp \
    computerrepos.cpp \
    computerservice.cpp \
    showsearchdialog.cpp \
    add_dialog.cpp \
    display_person_dialog.cpp \
    display_computer_dialog.cpp \
    utilities.cpp \
    addpconnectiondialog.cpp \
    linkerservice.cpp \
    linkerrepository.cpp \
    addcconnectiondialog.cpp \
    removedialog.cpp


HEADERS  += mainwindow.h \
    person.h \
    personservice.h \
    personrepos.h \
    computer.h \
    computerrepos.h \
    computerservice.h \
    showsearchdialog.h \
    add_dialog.h \
    display_person_dialog.h \
    display_computer_dialog.h \
    utilities.h \
    addpconnectiondialog.h \
    linkerservice.h \
    linkerrepository.h \
    addcconnectiondialog.h \
    removedialog.h


FORMS    += mainwindow.ui \
    showsearchdialog.ui \
    display_person_dialog.ui \
    display_computer_dialog.ui \
    addpconnectiondialog.ui \
    addcconnectiondialog.ui \
    add_dialog.ui \
    removedialog.ui






