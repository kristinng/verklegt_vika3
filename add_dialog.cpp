#include "add_dialog.h"
#include "ui_add_dialog.h"

ADD_Dialog::ADD_Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ADD_Dialog)
{
    ui->setupUi(this);
    ui->add_tab_widget->setCurrentIndex(0);
}

ADD_Dialog::~ADD_Dialog()
{
    delete ui;
}

void ADD_Dialog::on_Cancel_Button_clicked()
{
    this->close();
}
/* taking input from line edits
 * alot of the code is just input checks
 * then a call to addperson wich is explained in repo
 */
void ADD_Dialog::on_Addp_button_clicked()
{
    bool ErrorinAdd=false;
    person p = person();

    p.name = ui->input_pname->text().toStdString();
    if(Utilities.contains_number(p.name) || p.name.size()==0 ){
        ui->pname_valid->setText(QString::fromStdString("Invalid name"));
        ErrorinAdd = true;
    }

    if(ui->maleradiobutton->isChecked()){
        p.gender="Male";
    }
    else if(ui->femaleradiobutton->isChecked()){
        p.gender="Female";
    }
    string tempb;
    p.birthyear = ui->input_birthyear->text().toInt();
    tempb = ui->input_birthyear->text().toStdString();
    if(!Utilities.is_number(tempb) || tempb.size()==0){
        ui->birthyear_valid->setText(QString::fromStdString("Invalid birthyear"));
        ErrorinAdd = true;
    }
    string tempd;
    p.deathyear = ui->input_deathyear->text().toInt();
    tempd = ui->input_deathyear->text().toStdString();
    if(!Utilities.is_number(tempd)){
        ui->deathyear_valid->setText(QString::fromStdString("Invalid deathyear"));
        ErrorinAdd = true;
    }

    p.imagepath = ui->Pimagepath_line->text().toStdString();
    string confirmation="<span style='color: green'>Person successfully added </span>";

    try{
        Personservice.addperson(p);
    }catch(int e){
        ErrorinAdd=true;
    }

    if(ErrorinAdd==false){
        ui->input_pname->clear();
        ui->input_birthyear->clear();
        ui->input_deathyear->clear();
        ui->Pimagepath_line->clear();
    }

    else if(ErrorinAdd==true){
        confirmation="<span style='color: red'>Error occurred</span>";
    }
    ui->paddconfirmation->setText(QString::fromStdString(confirmation));
}
/*
 * making the browse button for image input
 */
void ADD_Dialog::on_imagepath_button_clicked()
{
    QString imagepath = QFileDialog::getOpenFileName(
                this,
                "Browse for image",
                "",
                "Image files (*.jpg *.png *.jpeg)");
    ui->Pimagepath_line->setText(imagepath);
}

void ADD_Dialog::on_c_imagepath_button_clicked()
{
    QString imagepath = QFileDialog::getOpenFileName(
                    this,
                    "Browse for image",
                    "",
                    "Image files (*.jpg *.png *.jpeg)");
    ui->Cimagepath_line->setText(imagepath);

}

void ADD_Dialog::on_C_Cancel_Button_clicked()
{
    this->close();
}
/*
 * same as the person one, alot of input checks and line edit reads
 * Then a call to add computer wich is explained in repo
 */
void ADD_Dialog::on_Addc_button__clicked()
{
    bool ErrorinAdd=false;
    computer c = computer();
    c.name = ui->input_cname->text().toStdString();
    if(Utilities.contains_number(c.name) || c.name.size()==0){
        ui->cname_valid->setText(QString::fromStdString("Invalid name"));
        ErrorinAdd = true;
    }
    c.type = ui->input_type->text().toStdString();
    if(Utilities.contains_number(c.type) || c.type.size()==0){
        ui->type_valid->setText(QString::fromStdString("Invalid type"));
        ErrorinAdd = true;
    }
    if(ui->Built_radiobutton->isChecked()){
        c.built=1;
    }
    else if(ui->Notbuilt_radiobutton->isChecked()){
        c.built=0;
    }
    string tempb;
    c.builtyear = ui->input_builtyear->text().toInt();

    tempb = ui->input_builtyear->text().toStdString();
    if(!Utilities.is_number(tempb)){
        ui->builtyear_valid->setText(QString::fromStdString("Invalid builtyear"));
        ErrorinAdd = true;
    }
    c.imagepath = ui->Cimagepath_line->text().toStdString();

    string confirmation="<span style='color: green'>computer successfully added </span>";
    try{
        Computerservice.addcomputer(c);
    }catch(int e){
        ErrorinAdd=true;
    }

    if(ErrorinAdd==false){
        ui->input_cname->clear();
        ui->input_type->clear();
        ui->input_builtyear->clear();
        ui->Cimagepath_line->clear();
    }

    else if(ErrorinAdd==true){
        confirmation="<span style='color: red'>Error occurred</span>";
    }
    ui->caddconfirmation->setText(QString::fromStdString(confirmation));
}
