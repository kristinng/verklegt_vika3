#include "removedialog.h"
#include "ui_removedialog.h"

removeDialog::removeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::removeDialog)
{
    ui->setupUi(this);
}

removeDialog::~removeDialog()
{
    delete ui;
}
/* 2 buttons for removing person or computer
 * both read from seperate lines
 */
void removeDialog::on_pushButton_remove_person_clicked()
{
    string removepersonname = ui->line_removeperson->text().toStdString();
    Personservice.removefromdb(removepersonname);
    ui->line_removeperson->clear();
}

void removeDialog::on_pushButton_remove_computer_clicked()
{
    string removecomputername = ui->line_removecomputer->text().toStdString();
    Computerservice.removefromdb(removecomputername);
    ui->line_removecomputer->clear();
}
