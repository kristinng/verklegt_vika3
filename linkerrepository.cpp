#include "linkerrepository.h"
#include <QDebug>

linkerrepository::linkerrepository()
{
    QString connectionName = "dataConnection";
    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("database.sqlite");
    }

}
/* finding the id's of the person and computer we want
 * then inserting them into our linker table
 */
void linkerrepository::connectCandP(person p,computer c){

    int tempCID=0, tempPID=0;

    db.open();

    QSqlQuery query(db);
    query.prepare("SELECT P.PersonID, C.ComputerID "
                  "FROM person P, computer C "
                  "WHERE P.Name = (:pname) AND P.Gender = (:gender) AND P.Birthyear = (:birthyear) AND P.Deathyear = (:deathyear) "
                  "AND C.Name = (:cname) AND C.Type = (:type) AND C.Built = (:built) AND C.Builtyear LIKE (:builtyear)");
    query.bindValue(":pname", QString::fromStdString(p.name));
    query.bindValue(":gender", QString::fromStdString(p.gender));
    query.bindValue(":birthyear", QString::number(p.birthyear));
    query.bindValue(":deathyear", QString::number(p.deathyear));
    query.bindValue(":cname", QString::fromStdString(c.name));
    query.bindValue(":type", QString::fromStdString(c.type));
    query.bindValue(":built", QString::number(c.built));
    query.bindValue(":builtyear", QString::number(c.builtyear));
    query.exec();
    QString d = getLastExecutedQuery(query);
    qDebug() << d;
    while(query.next()){
        tempCID = query.value("PersonID").toInt();
        tempPID = query.value("ComputerID").toInt();
    }


    QSqlQuery query2(db);
    query.prepare("INSERT INTO linker VALUES (:cid, :pid)");
    query.bindValue(":cid",tempCID);
    query.bindValue(":pid", tempPID);
    query.exec();
    db.close();
}
/* returns a list of all computers linked with
 * person p wich was passed through parameter
 */
list<computer> linkerrepository::getconnectedcomputers(person p){

    list<computer> connectedlist;
    db.open();
    QSqlQuery query(db);
    query.prepare("SELECT C.Name, C.Type, C.Built, C.Builtyear "
                  "FROM person P, linker L, computer C "
                  "WHERE P.PersonID = L.P_ID AND C.ComputerID = L.C_ID "
                  "AND P.Name = (:name) AND P.Gender = (:gender) AND P.Birthyear = (:birthyear) AND P.Deathyear LIKE (:deathyear)");
    query.bindValue(":name", QString::fromStdString(p.name));
    query.bindValue(":gender", QString::fromStdString(p.gender));
    query.bindValue(":birthyear", QString::number(p.birthyear));
    query.bindValue(":deathyear", QString::number(p.deathyear));
    query.exec();
    while(query.next()){
        computer c = computer();
        c.name = query.value("Name").toString().toStdString();
        c.type = query.value("Type").toString().toStdString();
        c.built = query.value("Built").toInt();
        c.builtyear = query.value("Builtyear").toInt();
        connectedlist.push_back(c);
    }
    db.close();
    return connectedlist;
}
/* same as above but
 * for list of persons
 */
list<person> linkerrepository::getconnectedpersons(computer c){
    list<person> connectedlist;
    db.open();
    QSqlQuery query(db);
    query.prepare("SELECT P.Name, P.Gender, P.Birthyear, P.Deathyear "
                  "FROM person P, linker L, computer C "
                  "WHERE P.PersonID = L.P_ID AND C.ComputerID = L.C_ID "
                  "AND C.Name = (:name) AND C.Type = (:type) AND C.Built = (:built) AND C.Builtyear = (:builtyear)");
    query.bindValue(":name", QString::fromStdString(c.name));
    query.bindValue(":type", QString::fromStdString(c.type));
    query.bindValue(":built", QString::number(c.built));
    query.bindValue(":builtyear", QString::number(c.builtyear));
    query.exec();
    QString d = getLastExecutedQuery(query);
    qDebug() << d;
    while(query.next()){
        person p = person();
        p.name = query.value("Name").toString().toStdString();
        p.gender = query.value("Gender").toString().toStdString();
        p.birthyear = query.value("Birthyear").toInt();
        p.deathyear = query.value("Deathyear").toInt();
        connectedlist.push_back(p);
    }
    db.close();
    return connectedlist;
}
/* same debugging function as seen before
 *
 */
QString linkerrepository::getLastExecutedQuery(const QSqlQuery& query)
{
 QString str = query.lastQuery();
 QMapIterator<QString, QVariant> it(query.boundValues());
 while (it.hasNext())
 {
  it.next();
  str.replace(it.key(),it.value().toString());
 }
 return str;
}
