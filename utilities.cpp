#include "utilities.h"
#include <iostream>
#include <string>

utilities::utilities()
{
}
// fall til að vera viss um að notandi inputtar staf ekki tölu/tákn
bool utilities::contains_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();

     while (it!=s.end()){
         if (std::isdigit(*it) || !(std::isalnum(*it)-std::isspace(*it))){

             return true;
         }
         it++;
     }
    return false;
}
// fall til að vera vis sum að notandi inputtar tölu ekki staf/tákn
bool utilities::is_number(const std::string &s){

    std::string::const_iterator it = s.begin();

     while (it!=s.end()){
         if (!std::isdigit(*it)){
             return false;
         }
         it++;
     }
    return true;
}
/*
bool utilities::is_empty(const std::string &s){
 std::string::const_iterator it = s.begin();
 while(it!=s.end()){
     it++;
 }
         if (QString(it).isEmpty()){
             return false;
         }
    return true;
}
*/
