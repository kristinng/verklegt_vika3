#ifndef DISPLAY_PERSON_DIALOG_H
#define DISPLAY_PERSON_DIALOG_H

#include <QDialog>
#include "person.h"
#include "personservice.h"
#include "addpconnectiondialog.h"

namespace Ui {
class display_person_Dialog;
}

class display_person_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit display_person_Dialog(QWidget *parent = 0);
    ~display_person_Dialog();

    void setperson(person Person);
    string currentname;
    string currentgender;
    int currentbirthyear;
    int currentdeathyear;
    linkerservice Linkerservice;

    void showconnected_computers();
private slots:


    void on_pushButton_addperson_connection_clicked();

private:
    Ui::display_person_Dialog *ui;
    personservice Personservice;
};

#endif // DISPLAY_PERSON_DIALOG_H
