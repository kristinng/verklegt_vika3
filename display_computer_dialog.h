#ifndef DISPLAY_COMPUTER_DIALOG_H
#define DISPLAY_COMPUTER_DIALOG_H

#include <QDialog>
#include "computer.h"
#include "computerservice.h"
#include "linkerservice.h"
#include "addcconnectiondialog.h"

namespace Ui {
class display_computer_dialog;
}

class display_computer_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit display_computer_dialog(QWidget *parent = 0);
    ~display_computer_dialog();
    linkerservice Linkerservice;
    string currentCname;
    string currenttype;
    int currentbuilt;
    int currentbuiltyear;

    void setcomputer(computer Computer);
    void showconnected_persons();
private slots:
    void on_pushButton_addcomputer_connection_clicked();

private:
    Ui::display_computer_dialog *ui;
    computerservice Computerservice;
};

#endif // DISPLAY_COMPUTER_DIALOG_H
